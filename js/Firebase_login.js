// Angular Function
$scope.signIn = function (email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password).then(function () {
        var user = firebase.auth().currentUser;
        user.getIdToken(/* forceRefresh */ true).then(function(idToken) {
            // Send token to your backend via HTTPS
            // ...
            console.log(idToken)
            var data = {idToken: idToken};
            $http({
                method: "POST",
                url: "/login",
                data: data
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);
            });
        }).catch(function(error) {
            // Handle error
        });
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
    });
}


// NodeJS /login POST route
app.post("/login", function (request, response) {
	var idToken = request.body.idToken;

	admin.auth().verifyIdToken(idToken).then(function(decodedToken) {
		var uid = decodedToken.uid;
		console.log(decodedToken)
		// ...
		response.json({success: true, decodedToken: decodedToken});
	}).catch(function(error) {
		// Handle error
	});

});
